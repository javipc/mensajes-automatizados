# Enter script code







class Mensajes:
    x = 1366
    y = 768
    textos = []
    horas = []
    salas = 1
    espera = 0.5
    tecla_enviar = "<enter>"
    
    dia = 0
    semana = 1
    
    def __init__ (self, textos = []):
        self.textos = textos
        
        
        
    def enviar_con_intro (self, valor = True):
        if (valor == True):
            self.tecla_enviar = "<enter>"
        else:
            self.tecla_enviar = "<ctrl>+<enter>"


            
    def enviar (self):
        keyboard.send_keys (self.tecla_enviar)
        time.sleep(self.espera)
        return
        boton = 1        
        ventana = window.get_active_geometry()
        x = ventana [2] - 10
        y = ventana [3] - 10
        mouse.click_absolute(x, y, boton)
        time.sleep(self.espera)
        return
        
    
    
    
    
    
    
    def introducir_fecha (self, dia = 0, semana = 1):
        if (dia == 0):
            return
        # esta función no está disponible en esta versión 
        return
        
    
    def introducir_hora (self, hora):
        time.sleep(self.espera)
        
        keyboard.send_key("<backspace>", 5)
        
        keyboard.send_keys (hora)
                        
        keyboard.send_key("<delete>", 3)
                    
        time.sleep(self.espera)
        
        

    
    def introducir_mensaje (self, mensaje):
        time.sleep(self.espera)
        keyboard.send_keys ("<ctrl>+a")
        keyboard.send_key ("<delete>")
        keyboard.send_keys (mensaje)
        time.sleep(self.espera)
    
    

    def publicar (self):
        horas = self.horas
        
        textos = []
        while (len(horas) > len(textos)):
            textos = textos + self.textos
        
        
        indice = 1
        
                 
        for hora in horas[1:]:
            self.introducir_mensaje (textos[indice])
            self.enviar()                
            self.introducir_fecha (self.dia, self.semana)
            self.introducir_hora (hora)
            keyboard.send_key ("<enter>")
            indice = indice + 1
        
    





class Horas:
    desde = 0
    hasta = 24
    cada = 1
    separador = ":"
    horas = []
    minutos = ["01"]
    lista = []
    
    def __init__ (self):
        hora = output = system.exec_command("date +%H")
        hora = int(hora) +1
        self.desde = hora
        
         

    def horas (self):        
        horas = range (self.desde ,self.hasta, self.cada)        
        return self.cadena (horas)
        
     
     
    def cadena (self, numeros):
        lista = []
        for numero in numeros:
            cero = ""
            if (numero < 10):
                cero = "0"
            lista.append (cero + str(numero))
        return lista
        
        
    def generar (self):
        horas = self.horas()
        self.horas = horas
        
        respuesta = []
        for hora in horas:
            for minuto in self.minutos:
                respuesta.append (hora + self.separador + minuto)
            
        self.lista = respuesta

        
    def obtener (self):
        self.generar ()
        return self.lista
        
        
    def ver (self):
        
        for hora in self.obtener():
            keyboard.send_keys (hora)
            keyboard.send_key ("<enter>")    
            
        






# aquí comienza tu configuración. ------------------------------------------


# carteles individuales

programacion = "Nuevo Grupo de Programación - Tecnología - Aplicaciones - https://t.me/Programador_ES "
publicidad = "https://t.me/SpamNoEsPublicidad "
publicidadlogo = "Yo sé que no lo van a entender pero los grupos de SPAM no existen. No es lo mismo Publicidad No Deseada que simplemente Publicidad. https://t.me/SpamNoEsPublicidad/846 "
publicidadseguime = "Localización de dispositivos Android con máxima privacidad y con código abierto. https://t.me/SpamNoEsPublicidad/148 "
kawaii = "La hora en fotografías. Consulta el historial de fotografías en el canal interactivo https://t.me/fotoreloj - Usa el robot @RelojBot para configurar tus preferencias. "
webtoon = "https://t.me/SpamNoEsPublicidad/2814 Enjoy this short story https://www.webtoons.com/en/challenge/he-crossed-time-for-me-h/list?title_no=470904 on WebToons "
webtoones = "https://t.me/SpamNoEsPublicidad/2814 Si te gusta el Manga te gustará esta breve historia: https://www.webtoons.com/en/challenge/he-crossed-time-for-me-h/list?title_no=470904 "
listagrama = "Verifica que tus canales, grupos y robots estén en listagrama https://t.me/listagrama "


# conjunto de carteles

textos = [programacion, webtoon, publicidadlogo,  kawaii, webtoon, programacion, publicidadseguime, webtoones, listagrama, lorene]


# ejemplo de horas

horas1 = ["0210", "0510", "0810", "1110", "1410", "1710", "2010", "2310"]
horas2 = ["0110", "0410", "0710", "1010", "1310", "1610", "1910", "2210"] 
horas3 = ["0310", "0610", "0910", "1210", "1510", "1810", "2110"]

# otros ejemplos de conjuntos de carteles
textos1 = [kawaii,listagrama, lorene]
textos2 = [programacion, publicidadseguime, programacion,publicidadlogo]
textos3 = [webtoones, webtoon]


horas = Horas()
horas.separador = ""
horas.minutos = ["10"]


mensajes = Mensajes ()

mensajes.salas = 1
mensajes.espera = 0.5


# estas opciones son obsoletas y no se utiliza en esta versión
# en la versión completa se puede programar un conjunto de días.
 
mensajes.dia = 4
mensajes.semana = 4



# aquí configura las horas y los textos

mensajes.horas = horas3
mensajes.textos = textos3
mensajes.publicar ()



