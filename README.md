# mensajes automatizados

Programador de mensajes para Telegram.

Este código te permite programar múltiples mensajes para ser emitidos en múltiples grupos, en múltiples fechas.
Utiliza la característica de mensajes programados de Telegram, por lo tanto una vez finalizado el programa no requiere conexión ni equipo, el propio servidor de Telegram se encarga de entregar los mensajes.

## Lo bueno.
Programación de varios textos con control de días y horas.
No requiere tener la computadora conectada.
No requiere clientes de terceros.
El código es visible y transparente (seguridad)
Si tienes dudas puedes consultar a tu amigo programador sobre el código.


## Lo malo.
Dependiendo de la cantidad de grupos y mensajes puede ser lento.
No permite utilizar la computadora mientras se ejecuta el código. 
Si no se vigila la ejecución del programa puede causar inundación (y expulsión) USAR CON CUIDADO Y VIGILAR EL PROGRAMA.
Solo disponible para autokey (por ahora).
No sube archivos (por ahora)

## Requisitos.

autokey:
https://github.com/autokey/autokey

telegram desktop:
https://desktop.telegram.org

Resolución:
1366 x 768






Este programa fue desarrollado en un sistema Mageia 8 (https://www.mageia.org) con escritorio KDE (https://kde.org) con una pantalla cuya Resolución es 1366 x 768 pixeles y la barra de tareas / sistema se encuentra en la parte superior.

Otras configuraciones puede no funcionar.


## Cómo se usa.
Abrir autokey.
Crear un nuevo archivo y pegar el código (o copiar el archivo al directorio de programas.
Configurar una tecla de acceso rápido.
Configurar las opciones del programa (carteles, horas, etc)
NO USAR NI HORA NI MINUTO CERO. Ej (10:00)


Maximizar Telegram.
Cerrar el panel lateral.
Ir a la sala para emitir mensajes.
Pulsar la teclas configuradas.

## IMPORTANTE: 
En la versión gratuita se debe presionar la tecla de acceso rápido dentro de la ventana de mensajes programados, para eso hay que programar al menos un mensaje de forma manual.



## Características:
Permite programar textos variados.
Permite seleccionar diferentes días.
Se puede publicar el múltiples grupos.
Utiliza un "texto borrador" para saber en que grupo debe publicar.
El programa se detiene haciendo clic en otro programa o quitando telegram.
Control de velocidad.
Minutos relativos o específicos.


## Recomendaciones.

VIGILAR EL PROGRAMA, no se debe dejarlo solo, puede causar inundaciones.
Usarlo lo más lento posible.
Utilizando virtualbox se puede dejarlo funcionado lento y así permite utilizar la computadora mientras el código trabaja.
Sacar provecho de la opción de textos múltiples para evitar ser repetitivo.
Utilizar las carpetas de Telegram para organizar los grupos.


# Más aplicaciones

Al no tener publicidad este proyecto se mantiene únicamente con donaciones.
Siguiendo este enlace tendrás más información y también más aplicaciones.
[Más información](https://gitlab.com/javipc/mas) 



